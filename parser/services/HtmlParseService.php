<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 19:48
 */

namespace parser\services;

use parser\entities\PlayerEntity;
use parser\entities\StatItemEntity as SIE;
use parser\entities\StatsEntity;
use parser\entities\TeamEntity;
use parser\models\StatItem;
use parser\repositories\PlayerRepository;
use parser\repositories\TeamRepository;
use Symfony\Component\DomCrawler\Crawler;
use parser\models\Player;
use parser\models\Team;
use yii\helpers\ArrayHelper as AH;
use yii\helpers\Url;

class HtmlParseService
{
    /***@var $crawler Crawler ***/
    public $crawler;

    /***@var $player PlayerEntity ***/
    private $player;

    /***@var $playerRepo PlayerRepository ***/
    private $playerRepo;

    /***@var $teamRepo TeamRepository ***/
    private $teamRepo;

    public function __construct()
    {
        $this->crawler = new Crawler();
        $this->player = new PlayerEntity();
        $this->playerRepo = new PlayerRepository();
        $this->teamRepo = new TeamRepository();
    }

    /***
     * @return PlayerEntity
     */
    public function getPlayer() : PlayerEntity
    {
        return clone ($this->player);
    }

    /***
     * @param $html
     */
    public function setHtmlData($html) : void
    {
        $this->crawler->add($html);
    }

    /***
     * @param $html
     * @param TeamEntity $team
     */
    public function parseTeamStats($html, TeamEntity $team) : void
    {
        $this->setHtmlData($html);

        $nodes = $this->crawler
            ->filter('div.columns div.standard-box div.large-strong')
            ->each(function (Crawler $node, $i){
                return $node->text();
            });

            $team->stats = (new StatsEntity([
                new SIE(SIE::TEAM_MAPS,AH::getValue($nodes,'0', null)),
                new SIE(SIE::TEAM_WINS, trim(explode('/',AH::getValue($nodes,'1', null))[0])),
                new SIE(SIE::TEAM_DRAWS, trim(explode('/',AH::getValue($nodes,'1', null))[1])),
                new SIE(SIE::TEAM_LOSES, trim(explode('/',AH::getValue($nodes,'1', null))[2])),
                new SIE(SIE::TEAM_KILLS,AH::getValue($nodes,'2', null)),
                new SIE(SIE::TEAM_DEATHS,AH::getValue($nodes,'3', null)),
                new SIE(SIE::TEAM_ROUNDS,AH::getValue($nodes,'4', null)),
                new SIE(SIE::TEAM_KD,AH::getValue($nodes,'5', null)),
            ]))->getAll();

            $this->teamRepo->save($team);
    }

    /***
     * @param $html
     * @param $team_id
     */
    public function parsePlayerStats($html,$team_id) : void
    {
        $this->setHtmlData($html);
        $table = $this->crawler->filter('table.stats-table ');
        $nodes = $table->filterXPath('//tr')
            ->each(function (Crawler $node, $i){
                $tds = $node->filter('td')
                    ->each(function (Crawler $node2, $y){
                        return $node2->text();
                });
                return $tds;
            });

        /*** del captions ***/
        unset($nodes[0]);

        foreach ($nodes as $node) {
            $player = $this->getPlayer();
            $player->nickName = AH::getValue($node,'0',null);
            $player->team_id = $team_id;
            $player->stats = (new StatsEntity([
                new SIE(SIE::PLAYER_MAPS, AH::getValue($node,'1',null)),
                new SIE(SIE::PLAYER_KDDIFF,AH::getValue($node,'2',null)),
                new SIE(SIE::PLAYER_KD,AH::getValue($node,'3',null)),
                new SIE(SIE::PLAYER_RATING,AH::getValue($node,'4',null)),
            ]))->getAll();

            $this->playerRepo->save($player);
        }
    }
}
