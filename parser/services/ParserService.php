<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 20:08
 */

namespace parser\services;


use parser\helpers\UrlParse;
use parser\models\ParseUrl;

class ParserService
{
    /***@var $httpService  HttpDownloadService ***/
    private $httpService;
    /***@var $htmlService HtmlParseService ***/
    private $htmlService;
    /***@var $urlParse UrlParse ***/
    private $urlParse;

    public function __construct(HttpDownloadService $httpService, HtmlParseService $html, UrlParse $urlParse)
    {
        $this->httpService = $httpService;
        $this->htmlService = $html;
        $this->urlParse = $urlParse;
    }

    /***
     * @param $url_id
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getData ($url_id) : void
    {
        $url = $this->getUrlByInternalID($url_id);

        if ($url!=' ') {
            $html = $this->httpService->download($url);
            if (strpos($url, 'players') !== false) {
                $this->htmlService->parsePlayerStats($html,$this->urlParse->getTeamId($url) );
            } else {
                $this->htmlService->parseTeamStats($html,$this->urlParse->getTeam($url));
            }
        }
    }

    /***
     * @param $url_id
     * @return string
     */
    private function getUrlByInternalID ($url_id) : string
    {
        $model = ParseUrl::find()->where(['id'=>$url_id])->limit(1)->one();
        if ($model) {
            return $model->url;
        }
        return ' ';
    }
}
