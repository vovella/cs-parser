<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 19:48
 */

namespace parser\services;

use yii\httpclient\Client;

class HttpDownloadService
{
    /*** @var $client Client ***/
    private $client;

    /***
     * HttpDownloadService constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /***
     * @param $url
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function download($url) : string
    {
        $request = $this->client
            ->createRequest()
            ->setMethod('GET')
            ->setUrl($url);
        $response = $request->send();
        if ($response->isOk) {
            return $response->content;
        }
        return ' ';
    }
}
