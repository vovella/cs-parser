<?php

namespace parser\models;

use Yii;

/**
 * This is the model class for table "player_stats".
 *
 * @property int $player_id
 * @property int $stat_item_id
 * @property string $value
 *
 * @property Player $player
 * @property StatItem $statItem
 */
class PlayerStat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'player_stats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['player_id', 'stat_item_id'], 'integer'],
            [['value'], 'required'],
            [['value'], 'string', 'max' => 255],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Player::className(), 'targetAttribute' => ['player_id' => 'id']],
            [['stat_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatItem::className(), 'targetAttribute' => ['stat_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'player_id' => 'Player ID',
            'stat_item_id' => 'Stat Item ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Player::className(), ['id' => 'player_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatItem()
    {
        return $this->hasOne(StatItem::className(), ['id' => 'stat_item_id']);
    }

    public function fields()
    {
        return [
            'stat_item' => function() { return $this->statItem->name; },
            'value' =>'value',
        ];
    }
}
