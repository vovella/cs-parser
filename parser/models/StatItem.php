<?php

namespace parser\models;

use Yii;

/**
 * This is the model class for table "stat_items".
 *
 * @property int $id
 * @property int $entity_id
 * @property string $name
 *
 * @property PlayerStat[] $playerStats
 * @property TeamStat[] $teamStats
 */
class StatItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stat_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_id' => 'Entity ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerStats()
    {
        return $this->hasMany(PlayerStat::className(), ['stat_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamStats()
    {
        return $this->hasMany(TeamStat::className(), ['stat_item_id' => 'id']);
    }
}
