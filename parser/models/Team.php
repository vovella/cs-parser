<?php

namespace parser\models;

use Yii;

/**
 * This is the model class for table "teams".
 *
 * @property int $id
 * @property string $name
 *
 * @property Player[] $players
 * @property TeamStat[] $teamStats
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teams';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamStats()
    {
        return $this->hasMany(TeamStat::className(), ['team_id' => 'id']);
    }

    public function extraFields()
    {
        return [
            'players' => 'players' ,
        ];
    }

    public function fields()
    {
        return [
            'name' => 'name',
            'overview' => 'teamStats'
        ];
    }
}
