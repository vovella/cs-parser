<?php

namespace parser\models;

use Yii;

/**
 * This is the model class for table "players".
 *
 * @property int $id
 * @property int $team_id
 * @property string $nick_name
 *
 * @property PlayerStat[] $playerStats
 * @property Team $team
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'players';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id'], 'integer'],
            [['nick_name'], 'string', 'max' => 255],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'team_id' => 'Team ID',
            'nick_name' => 'Nick Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerStats()
    {
        return $this->hasMany(PlayerStat::className(), ['player_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    public function fields()
    {
        return [
            'nickname' => 'nick_name',
            'overview' =>'playerStats',
        ];
    }


}
