<?php

namespace parser\models;

use Yii;

/**
 * This is the model class for table "team_stats".
 *
 * @property int $team_id
 * @property int $stat_item_id
 * @property string $value
 *
 * @property StatItem $statItem
 * @property Team $team
 */
class TeamStat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team_stats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['team_id', 'stat_item_id'], 'integer'],
            [['value'], 'required'],
            [['value'], 'string', 'max' => 255],
            [['stat_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatItem::className(), 'targetAttribute' => ['stat_item_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'TeamEntity ID',
            'stat_item_id' => 'Stat Item ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatItem()
    {
        return $this->hasOne(StatItem::className(), ['id' => 'stat_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    public function fields()
    {
        return [
            'stat_item' => function() { return $this->statItem->name; },
            'value' =>'value',
        ];
    }
}
