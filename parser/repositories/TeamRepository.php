<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 25.02.2019
 * Time: 0:30
 */

namespace parser\repositories;

use parser\entities\TeamEntity;
use parser\models\Team;
use parser\models\TeamStat;
use parser\repositories\TeamStatsRepository;
use yii\helpers\Url;

class TeamRepository
{
    /***@var $teamStatItemRepo TeamStatsRepository ***/
    private $teamStatItemRepo;

    public function __construct()
    {
        $this->teamStatItemRepo = new TeamStatsRepository();
    }

    /***
     * @param TeamEntity $team
     */
    public function save(TeamEntity $team) : void
    {
        $exists = Team::find()->where( [ 'id' => $team->id ] )->exists();

        if($exists) {
            $this->saveStats($team);
        } else {
            $model = new Team(['id'=>$team->id,'name'=>$team->name]);
            $model->save();
            $this->saveStats($team);
        }

        \Yii::$app->controller->redirect(Url::to(['teams/'.$team->id]));
    }

    private function saveStats(TeamEntity $team):void
    {
        foreach ($team->stats as $item){
            $this->teamStatItemRepo->save($item, $team->id);
        }
    }
}