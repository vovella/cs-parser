<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 26.02.2019
 * Time: 19:28
 */

namespace parser\repositories;


use parser\entities\StatItemEntity;
use parser\models\PlayerStat;
use yii\db\ActiveRecord;
use yii\helpers\Url;

class PlayerStatsRepository
{
    /*** @var $model PlayerStat ***/
    private $model;

    public function __construct()
    {
        $this->model = new PlayerStat();
    }

    /***
     * @param $player_id
     * @param $stat_item_id
     * @param $value
     * @return PlayerStat
     */
    public function create($player_id,$stat_item_id,$value) : PlayerStat
    {
        $model = clone $this->model;
        $model->player_id = $player_id;
        $model->stat_item_id = $stat_item_id;
        $model->value = $value;

        return $model;
    }

    /***
     * @param PlayerStat $model
     */
    public function save(PlayerStat $model) : void
    {
        if ($this->isExist($model->stat_item_id, $model->player_id)) {
            //update models
        } else {
            $this->saveModel($model);
        }
    }

    /***
     * @param $id
     * @param $player_id
     * @return bool
     */
    public function isExist ($id, $player_id ) : bool
    {
        return $this->model::find()->where(['player_id' => $player_id,'stat_item_id'=>$id])->exists();
    }

    /***
     * @param ActiveRecord $model
     * @return bool
     */
    private function saveModel(ActiveRecord $model) : bool
    {
        if ($model->save()) {
            return true;
        }
        if ($model->hasErrors()) {
            var_dump($model->getErrors());
        }
        return false;
    }
}
