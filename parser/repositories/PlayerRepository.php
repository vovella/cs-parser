<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 22:39
 */

namespace parser\repositories;

use parser\entities\PlayerEntity;
use parser\models\Player;
use parser\models\Team;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class PlayerRepository
{
    /*** @var $errors array */
    private $errors;

    /* @var $temReop PlayerStatsRepository */
    private $itemRepo;
    
    public function __construct()
    {
        $this->itemRepo = new PlayerStatsRepository();
    }

    /***
     * @param PlayerEntity $player
     */
    public function save(PlayerEntity $player) : void
    {
        if (!$this->isExist($player)) {
            $playerModel = $this->getPlayerModel();
            $playerModel->team_id = $player->team_id;
            $playerModel->nick_name = $player->nickName;

            $this->saveModel($playerModel, $player->stats);
        } else {
            //update models if needed
        }

        \Yii::$app->controller->redirect(Url::to(['teams/'.$player->team_id.'?expand=players']));
    }

    /***
     * @param PlayerEntity $player
     * @return bool
     */
    public function isExist (PlayerEntity $player) : bool
    {
        $model = $this->getPlayerModel();
        return $model::find()->where(['team_id'=> $player->team_id, 'nick_name' => $player->nickName])->exists();
    }

    /***
     * @return ActiveRecord
     */
    private function getPlayerModel() : ActiveRecord
    {
        return new Player();
    }

    /***
     * @param ActiveRecord $model
     * @return bool
     */
    private function saveModel(ActiveRecord $model, $stats=[]) : bool
    {
        if ($model->save()) {
            foreach ($stats as $item) {
                $playerStatItem = $this->itemRepo->create($model->id, $item->type, $item->value);
                $this->itemRepo->save($playerStatItem);

            }
            return true;
        }
        if ($model->hasErrors()) {
            ArrayHelper::setValue($this->errors,'model', $model->getErrors());
        }
        return false;
    }
}
