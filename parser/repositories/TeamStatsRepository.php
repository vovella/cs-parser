<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 25.02.2019
 * Time: 1:00
 */

namespace parser\repositories;


use parser\entities\StatItemEntity;
use parser\models\TeamStat;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class TeamStatsRepository
{
    /***@var $model ActiveRecord ***/
    private $model;

    public function __construct()
    {
        $this->model = new TeamStat();
    }

    /***
     * @param StatItemEntity $item
     * @param $team_id
     */
    public function save(StatItemEntity $item, $team_id) : void
    {
        if ($this->isExist($item->type, $team_id)) {
            // update models if needed
        } else {
            $model = new TeamStat(['team_id'=>$team_id,'stat_item_id'=>$item->type, 'value'=>$item->value]);
            $model->save();
        }
    }

    /***
     * @param $id
     * @param $team_id
     * @return bool
     */
    private function isExist($id, $team_id) : bool
    {
        $exists = $this->model::find()->where( ['team_id' => $team_id,'stat_item_id'=>$id])->exists();
        if ($exists) {
            return true;
        }
        return false;
    }
}
