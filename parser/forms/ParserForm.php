<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 11:00
 */

namespace parser\forms;

use Yii;
use yii\base\Model;

class ParserForm extends Model
{
    public $url;

    public function rules()
    {
        return [
            [[ 'url'], 'required'],
        ];
    }
}