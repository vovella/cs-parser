<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 23:41
 */

namespace parser\helpers;


use parser\entities\TeamEntity;

class UrlParse
{
    public function getTeamId($url) : int
    {
        $parts = explode('/',$url);
        foreach ($parts as $item) {
            if(is_numeric($item)){
                return $item;
            }
        }
        return 0;
    }

    public function getTeam($url) : TeamEntity
    {
        $parts = array_reverse(explode('/',$url));
        return new TeamEntity($parts[1],$parts[0]);

    }
}