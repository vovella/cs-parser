<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 23:13
 */

namespace parser\entities;


class StatItemEntity
{
    const TEAM = 1;
    const PLAYER = 2;

    const TEAM_MAPS     = 1;
    const TEAM_WINS     = 2;
    const TEAM_DRAWS    = 3;
    const TEAM_LOSES    = 4;
    const TEAM_KILLS    = 5;
    const TEAM_DEATHS   = 6;
    const TEAM_ROUNDS   = 7;
    const TEAM_KD       = 8;
    const PLAYER_MAPS   = 9;
    const PLAYER_KDDIFF = 10;
    const PLAYER_KD     = 11;
    const PLAYER_RATING = 12;

    /***@var $type int ***/
    public $type;

    /***@var $value string ***/
    public $value;

    public function __construct(int $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
    }
}