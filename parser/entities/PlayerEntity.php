<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 1:09
 */

namespace parser\entities;


class PlayerEntity
{
    /***@var $nickName string ***/
    public $nickName;

    /***@var $team_id int ***/
    public $team_id;

    /***@var $stats array ***/
    public $stats=[];

}