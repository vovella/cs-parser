<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 24.02.2019
 * Time: 23:12
 */

namespace parser\entities;

use parser\entities\StatItemEntity;

class StatsEntity
{
    /*** @var $stats []***/
    private $stats = [];

    public function __construct(array $stats)
    {
        foreach ($stats as $statItem) {
            $this->add($statItem);
        }
    }

    /***
     * @param \parser\entities\StatItemEntity $statItem
     */
    public function add (StatItemEntity $statItem) : void
    {
        $this->stats[] = $statItem;
    }

    /***
     * @return array
     */
    public function getAll():array
    {
        return $this->stats;
    }
}
