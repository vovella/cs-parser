<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 23.02.2019
 * Time: 23:02
 */

namespace parser\entities;


class TeamEntity
{
    /***@var $id int ***/
    public $id;

    /***@var $name string ***/
    public $name;

    /***@var $stats array ***/
    public $stats=[];

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
