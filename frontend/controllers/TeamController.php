<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 23.02.2019
 * Time: 22:45
 */

namespace frontend\controllers;

use yii\rest\ActiveController;

class TeamController extends ActiveController
{
    public $modelClass = 'parser\models\Team';
}