<?php
/**
 * Created by PhpStorm.
 * User: voven
 * Date: 26.02.2019
 * Time: 20:35
 */

namespace frontend\controllers;

use yii\rest\ActiveController;

class PlayerController extends ActiveController
{
    public $modelClass = 'parser\models\Player';
}