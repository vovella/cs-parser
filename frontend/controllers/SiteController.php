<?php
namespace frontend\controllers;

use parser\models\ParseUrl;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Module;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use parser\services\ParserService;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private $parserService;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, ParserService $parserService, array $config = [])
    {
        $this->parserService = $parserService;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ParseUrl();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Successfully saved');
            return $this->redirect('');
        }
        if ($model->hasErrors()) {
            Yii::$app->session->setFlash('error', 'Houston, we have a Problem');
        }
        return $this->render('index',['model'=>$model]);
    }


    public function actionParser($id)
    {
        $this->parserService->getData($id);

    }

}
