<?php

use yii\db\Migration;

/**
 * Class m190223_210115_players
 */
class m190223_210115_players extends Migration
{
    public $tableName = '{{%players}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'team_id' => $this->integer(), /*may be player have not a team at the moment*/
            'nick_name' => $this->string(),
        ], $tableOptions);

        $this->alterColumn($this->tableName, 'team_id', 'integer');
        $this->createIndex(
            'index_team_id',
            $this->tableName,
            'team_id'
        );
        $this->addForeignKey(
            'fk_team_id',
            $this->tableName,
            'team_id',
            'teams',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_team_id',$this->tableName);
        $this->dropIndex('index_team_id',$this->tableName);
        $this->dropTable($this->tableName);
    }
}
