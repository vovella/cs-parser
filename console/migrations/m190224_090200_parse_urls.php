<?php

use yii\db\Migration;

/**
 * Class m190224_090200_parse_urls
 */
class m190224_090200_parse_urls extends Migration
{
    public $tableName = '{{%parse_urls}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
        ], $tableOptions);

        $this->execute("ALTER TABLE `parse_urls` ADD UNIQUE INDEX `index_unique_url` (`url`) USING BTREE;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
