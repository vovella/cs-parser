<?php

use yii\db\Migration;

/**
 * Class m190224_205342_team_stats
 */
class m190224_205342_team_stats extends Migration
{
    public $tableName = '{{%team_stats}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName, [
            'team_id' => $this->integer()->notNull(),
            'stat_item_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->alterColumn($this->tableName, 'team_id', 'integer');
        $this->createIndex(
            'index_team_id2',
            $this->tableName,
            'team_id'
        );
        $this->addForeignKey(
            'fk_team_id2',
            $this->tableName,
            'team_id',
            'teams',
            'id',
            'SET NULL'
        );
        $this->alterColumn($this->tableName, 'stat_item_id', 'integer');
        $this->createIndex(
            'index_stat_item_id2',
            $this->tableName,
            'stat_item_id'
        );
        $this->addForeignKey(
            'fk_stat_item_id2',
            $this->tableName,
            'stat_item_id',
            'stat_items',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_team_id2',$this->tableName);
        $this->dropIndex('index_team_id2',$this->tableName);
        $this->dropForeignKey('fk_stat_item_id2',$this->tableName);
        $this->dropIndex('index_stat_item_id2',$this->tableName);
        $this->dropTable($this->tableName);
    }
}
