<?php

use yii\db\Migration;

/**
 * Class m190224_204728_player_stats
 */
class m190224_204728_player_stats extends Migration
{
    public $tableName = '{{%player_stats}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName, [
            'player_id' => $this->integer()->notNull(),
            'stat_item_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
        ], $tableOptions);

        $this->alterColumn($this->tableName, 'player_id', 'integer');
        $this->createIndex(
            'index_player_id',
            $this->tableName,
            'player_id'
        );
        $this->addForeignKey(
            'fk_player_id',
            $this->tableName,
            'player_id',
            'players',
            'id',
            'CASCADE'
        );
        $this->alterColumn($this->tableName, 'stat_item_id', 'integer');
        $this->createIndex(
            'index_stat_item_id',
            $this->tableName,
            'stat_item_id'
        );
        $this->addForeignKey(
            'fk_stat_item_id',
            $this->tableName,
            'stat_item_id',
            'stat_items',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_player_id',$this->tableName);
        $this->dropIndex('index_player_id',$this->tableName);
        $this->dropForeignKey('fk_stat_item_id',$this->tableName);
        $this->dropIndex('index_stat_item_id',$this->tableName);
        $this->dropTable($this->tableName);
    }
}
