<?php

use yii\db\Migration;

/**
 * Class m190223_230119_stat_items
 */
class m190223_230119_stat_items extends Migration
{
    public $tableName = '{{%stat_items}}';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'entity_id' => $this->integer(),
            'name' => $this->string(),
        ], $tableOptions);
        $this->batchInsert($this->tableName,['entity_id','name'],[
            ['1','Maps played'],
            ['1','Wins'],
            ['1','Draws'],
            ['1','Loses'],
            ['1','Total kills'],
            ['1','Total deaths'],
            ['1','Rounds played'],
            ['1','K/D Ratio'],
            ['2','Maps'],
            ['2','K/D Diff'],
            ['2','K/D'],
            ['2','Rating'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
